package com.kangjusang.lifecycleobservertest;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

import static androidx.lifecycle.Lifecycle.State.STARTED;

public class DummyClassObservingLifecycle implements LifecycleObserver {

    private boolean enabled = false;
    private Context mContext;

    public DummyClassObservingLifecycle(Context context) {
        if (context instanceof Application)
            Log.d("DummyClass", "context is instanceof Application");
        else if (context instanceof Activity)
            Log.d("DummyClass", "context is instanceof Activity");
        else
            Log.d("DummyClass", "context is instanceof Context");
        mContext = context;
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    void create() {
        Toast.makeText(getApplicationContext(mContext), "ON_CREATE", Toast.LENGTH_SHORT).show();
        Log.d("DummyClass", "ON_CREATE");
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    void start() {
        Toast.makeText(getApplicationContext(mContext), "ON_START", Toast.LENGTH_SHORT).show();
        Log.d("DummyClass", "ON_START");
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    void resume() {
        Toast.makeText(getApplicationContext(mContext), "ON_RESUME", Toast.LENGTH_SHORT).show();
        Log.d("DummyClass", "ON_RESUME");
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    void pause() {
        Toast.makeText(getApplicationContext(mContext), "ON_PAUSE", Toast.LENGTH_SHORT).show();
        Log.d("DummyClass", "ON_PAUSE");
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    void stop() {
        Toast.makeText(getApplicationContext(mContext), "ON_STOP", Toast.LENGTH_SHORT).show();
        Log.d("DummyClass", "ON_STOP");
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    void destroy() {
        Toast.makeText(getApplicationContext(mContext), "ON_DESTROY", Toast.LENGTH_SHORT).show();
        Log.d("DummyClass", "ON_DESTROY");
    }

//    @OnLifecycleEvent(Lifecycle.Event.ON_ANY)
//    void any() {
//        Toast.makeText(getApplicationContext(mContext), "ON_ANY", Toast.LENGTH_SHORT).show();
//        Log.d("DummyClass", "ON_ANY");
//    }

    private Context getApplicationContext(Context mContext) {
        if (!(mContext instanceof Application))
            return mContext.getApplicationContext();
        return mContext;
    }
}
